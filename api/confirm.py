# flask packages
from flask import Response, request
from flask_restful import Resource

# project resources_legacy
from helper.h_confirm import get_confirm, post_confirm_by_email, delete_confirm, post_confirm_by_phone


class ConfirmApi(Resource):
    """ here is the api endpoint to get, post and delete organizations record"""
    def get(self) -> Response:
        data =dict(request.args)
        return get_confirm(data['pincode'], data['email'])

    def post(self) -> Response:
        data = request.get_json(force=True)
        if '/confirm/through_email' in request.url:
            return post_confirm_by_email(data)
        elif '/confirm/through_phone' in request.url:
            return post_confirm_by_phone(data)


    def delete(self) -> Response:
        return delete_confirm()
