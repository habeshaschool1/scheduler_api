# flask packages
from flask import Response, request
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource

# project resources_legacy
from api.response_messages import forbidden, client_error
from helper.constants import CUser
from helper.h_districts import delete_district, put_district, post_district, get_districts
from helper.h_organizations import get_organizations, post_organization, delete_organization, put_organization
from models.confirm import Confirm
from models.users import Users


class DistrictsApi(Resource):
    """ here is the api endpoint to get, post and delete organizations record"""
    def get(self) -> Response:
        args = dict(request.args)
        return get_districts(query=args)

    def post(self) -> Response:
        data = request.get_json(force=True)
        pincode = data.pop('pincode', False)
        if pincode: # try if request is from a person who create new org.
            try:  # if initiated by a system user
                confirm = Confirm.objects.get(pincode=pincode)
                data['info'] = confirm.email
                return post_district(data)
            except Exception as e:
                return client_error(f"{e}")
        else:  # or else only system user is allowed to do so
            try:
                return post_district(data)

                """users = Users.objects()
                current_user = users.get(id=get_jwt_identity())
                if current_user.access == CUser().ACCESS_SYSTEM_USER:
                    data['info'] = current_user.email
                    return post_district(data)
                else:
                    return client_error("You are not authorized to create district")
                    """
            except Exception as e:
                return client_error(f"{e}")


    @jwt_required()
    def put(self, district_id: int) -> Response:
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to update districts
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                data = request.get_json(force=True)
                return put_district(district_id, data)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")

    @jwt_required()
    def delete(self, district_id: int) -> Response:
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to delete districts
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                return delete_district(district_id)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")