# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from api.response_messages import forbidden, post_success, delete_success, not_found, client_error
# project resources_legacy
from helper.constants import CUser
from helper.h_users import put_user, get_users, get_users_public_info, get_current_user, add_token_to_block_list, \
    refresh_token, change_password, login, get_new_user_id, post_user
from helper.operate import get_random_int
from models.users import Users

c_user = CUser()


class UsersApi(Resource):

    @jwt_required()
    def get(self, user_id: int=None) -> Response:
        """
        GET response method for acquiring single user data.
        JSON Web Token is required.
        Authorization is required: Access(admin=true) or UserId = get_jwt_identity()

        :return: JSON object
        """
        args = dict(request.args)
        if "/users/public_info" in request.url:
            return get_users_public_info(query=args)
        elif "/current_user" in request.url:
            return get_current_user()
        else:
            return get_users(user_id, query=args)

    @staticmethod
    def post() -> Response:
        """
        POST response method for creating user.
        :return: JSON object
        """
        data = request.get_json(force=True)
        data["user_id"] = get_new_user_id()
        return post_user(data)

    @jwt_required()
    def put(self, user_id: int) -> Response:
        """
        update user
        :return: JSON object
        """
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # user profile can be updated either admin user or its own user
            if int(current_user.user_id) == int(user_id) or current_user.access == CUser().ACCESS_ADMIN_USER:
                data = request.get_json(force=True)
                # password can't be modified through here
                data.pop("password", None)
                return put_user(user_id=user_id, data=data)
            else:
                return forbidden(msg=f"you must be owner or admin to update user with user_id ={user_id}")
        except:
            return not_found(msg=f"user_id = {user_id} is not available")

    @jwt_required()
    def delete(self, user_id: int) -> Response:
        """
        DELETE response method for deleting user.
        JSON Web Token is required.
        Authorization is required: Access(admin=true)

        :return: JSON object
        """
        current_user = Users.objects.get(id=get_jwt_identity())
        if current_user.access == CUser.ACCESS_ADMIN_USER:
            output = Users.objects(user_id=user_id).delete()
            return delete_success(result=output)
        else:
            return forbidden()


class LoginApi(Resource):
    """
        method to login to your email and password
    """
    @staticmethod
    def post() -> Response:
        """
        POST response method for retrieving user web token.
        :return: JSON object
        """
        # check usr name and password
        data = request.get_json(force=True)
        return login(data)


class ChangePasswordApi(Resource):
    """
        method to change user password
        you must be either admin user or
        owner of the logged in user
    """
    @staticmethod
    @jwt_required
    def put(user_id: int) -> Response:
        """
        only admin user or owner of the account may change password
        chek if user is either admin or owner of the account
        check old password matches
        check new password fulfil the requirement
        change password"""
        try:
            # check user is admin or owner of the given user_id
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # user profile can be updated either admin user or its own user
            if current_user.user_id == user_id or current_user.access == CUser().ACCESS_ADMIN_USER:
                data = request.get_json(force=True)
                return change_password(user_id, data.get("old_password", ""), data.get("new_password", ""))
            else:
                return forbidden(msg=f"you must be owner or admin to change password of user_id ={user_id}")
        except:
            return not_found(msg=f"user_id = {user_id} is not available")


class LogoutApi(Resource):
    @staticmethod
    @jwt_required()
    def delete() -> Response:
        return add_token_to_block_list()


class EmployeeApi(Resource):
    @jwt_required()
    def post(self) -> Response:
        data = request.get_json(force=True)
        users = Users.objects()
        current_user = users.get(id=get_jwt_identity())
        if current_user.access not in [c_user.ACCESS_SYSTEM_USER, c_user.ACCESS_ADMIN_USER]:
            return forbidden(msg=f"you are not authorized to add new employee")

        # check if the new user email and organization_id already exists
        user = Users.objects(email=data['email'], organization_id=current_user.organization_id)
        if user:
            return client_error(msg="user already exist to this organization", result=user)

        data["user_id"] = get_new_user_id()
        data["access"] = c_user.ACCESS_NORMAL_USER
        data['organization_id'] = current_user.organization_id
        return post_user(data)

    @jwt_required()
    def delete(self, user_id: int) -> Response:
        """
        DELETE response method for deleting user.
        JSON Web Token is required.
        Authorization is required: Access(admin=true)

        :return: JSON object
        """
        current_user = Users.objects.get(id=get_jwt_identity())
        if current_user.access not in (c_user.ACCESS_SYSTEM_USER, c_user.ACCESS_ADMIN_USER):
            return forbidden(msg=f"you must be admin user to to delete users")
        user_tobe_deleted = Users.objects.get(user_id=user_id)

        if current_user.organization_id != user_tobe_deleted.organization_id:
            return forbidden(msg=f"You are not admin to this organization to delete user")

        output = Users.objects(user_id=user_id).delete()
        return delete_success(result=output)