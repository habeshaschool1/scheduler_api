# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

# project resources_legacy
from api.response_messages import forbidden, not_found, client_error
from helper.constants import CUser
from helper.h_emails import post_email
from models.users import Users


class EmailApi(Resource):

    @jwt_required()
    def post(self) -> Response:
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to update categories
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                data = request.get_json(force=True)
                return post_email(data)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")
