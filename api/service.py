# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

# project resources_legacy
from api.response_messages import client_error, forbidden
from helper.constants import CUser
from helper.h_services import get_services, post_service, delete_service, put_service
from models.users import Users


class ServicesApi(Resource):
    """ here is the api endpoint to get, post and delete services record"""
    def get(self, service_id=None) -> Response:
        args = dict(request.args)
        return get_services(service_id, query=args)

    @jwt_required()
    def post(self) -> Response:
        data = request.get_json(force=True)
        organization_id = int(data['organization_id'])
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to delete service
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                return post_service(data)
            elif current_user.access == CUser().ACCESS_ADMIN_USER and current_user.organization_id == organization_id:
                return post_service(data)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")

    @jwt_required()
    def put(self, service_id: int) -> Response:
        data = request.get_json(force=True)
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to delete service
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                return put_service(service_id, data)
            elif current_user.access == CUser().ACCESS_ADMIN_USER and service_id.startswith(f"{current_user.organization_id}."):
                return put_service(service_id, data)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")

    @jwt_required()
    def delete(self, service_id: int) -> Response:
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to delete service
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                return delete_service(service_id)
            elif current_user.access == CUser().ACCESS_ADMIN_USER and service_id.startswith(f"{current_user.organization_id}."):
                return delete_service(service_id)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")