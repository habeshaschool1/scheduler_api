# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

# project resources_legacy
from api.response_messages import forbidden, client_error
from helper.constants import CUser
from helper.h_organizations import get_organizations, post_organization, delete_organization, put_organization
from models.users import Users


class OrganizationsApi(Resource):
    """ here is the api endpoint to get, post and delete organizations record"""
    def get(self, organization_id: int = None) -> Response:
        args = dict(request.args)
        return get_organizations(organization_id= organization_id, query=args)

    def post(self) -> Response:
        data = request.get_json(force=True)
        return post_organization(data)

    @jwt_required()
    def put(self, organization_id: int) -> Response:
        try:
            organization_id = int(organization_id)
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to delete categories
            if current_user.access == CUser().ACCESS_SYSTEM_USER or (current_user.access == CUser().ACCESS_ADMIN_USER and current_user.organization_id == organization_id):
                data = request.get_json(force=True)
                return put_organization(organization_id, data)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")

    @jwt_required()
    def delete(self, organization_id: int) -> Response:
        try:
            organization_id = int(organization_id)
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to other categories
            if current_user.access == CUser().ACCESS_SYSTEM_USER and current_user.organization_id != organization_id:
                return delete_organization(organization_id)
            elif current_user.access == CUser().ACCESS_ADMIN_USER and current_user.organization_id == organization_id:
                return delete_organization(organization_id)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")