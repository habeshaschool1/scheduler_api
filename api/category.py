# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

# project resources_legacy
from api.response_messages import forbidden, not_found, client_error
from helper.constants import CUser
from helper.h_categories import get_category, post_category, put_category, \
    delete_category
from helper.h_organizations import get_organizations, post_organization, delete_organization, put_organization
from models.confirm import Confirm
from models.users import Users


class CategoriesApi(Resource):
    """ here is the api endpoint to get, post and delete organizations record"""
    def get(self) -> Response:
        args =dict(request.args)
        return get_category(query=args)

    def post(self) -> Response:
        data = request.get_json(force=True)
        pincode = data.pop('pincode', False)
        if pincode: # try if request is from a person who create new org.
            try:  # if initiated by a system user
                confirm = Confirm.objects.get(pincode=pincode)
                data['info'] = confirm.email
                return post_category(data)
            except Exception as e:
                return client_error(f"{e}")
        else:  # or else only system user is allowed to do so
            try:
                return post_category(data)

                """users = Users.objects()
                current_user = users.get(id=get_jwt_identity())
                if current_user.access == CUser().ACCESS_SYSTEM_USER:
                    data['info'] = current_user.email
                    return post_category(data)
                else:
                    return client_error("You are not authorized to create category")
                    """
            except Exception as e:
                return client_error(f"{e}")

    @jwt_required()
    def put(self, category_id: int) -> Response:
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to update categories
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                data = request.get_json(force=True)
                return put_category(category_id, data)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")

    @jwt_required()
    def delete(self, category_id: int) -> Response:
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to delete categories
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                return delete_category(category_id)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")