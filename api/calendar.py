# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

# project resources_legacy
from api.response_messages import forbidden, client_error
from helper.constants import CUser
from helper.h_calendar import get_calendar, post_calendar, post_calendar_ethiopia, get_calendar_ethiopia
from models.users import Users


class CalendarApi(Resource):
    """ here is the api endpoint to get, post and delete organizations record"""
    def get(self) -> Response:
        args = dict(request.args)
        if "ethiopia" in request.url.lower():
            return get_calendar_ethiopia(query=args)
        else:
            return get_calendar(query=args)

    @jwt_required()
    def post(self) -> Response:
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to update calendar
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                return post_calendar()
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")
