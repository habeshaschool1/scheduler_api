# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

# project resources_legacy
from api.response_messages import forbidden, client_error
from helper.constants import CUser
from helper.h_comments import post_comment, delete_comment, get_comments
from models.users import Users


class CommentsApi(Resource):
    """ here is the api endpoint to get, post and delete organizations record"""
    def get(self) -> Response:
        data =dict(request.args)
        return get_comments(data)

    def post(self) -> Response:
        data = request.get_json(force=True)
        return post_comment(data)

    @jwt_required()
    def delete(self, _id: int) -> Response:
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # only system user is able to delete comments
            if current_user.access == CUser().ACCESS_SYSTEM_USER:
                return delete_comment(_id)
            else:
                return forbidden(msg=f"you have no right to do this")
        except Exception as e:
            return client_error(msg=f"{e}")