# flask packages
from flask import Response, request
from flask_restful import Resource

# project resources_legacy
from helper.h_appointment import get_appointment, put_appointments, delete_appointment
from helper.h_appointments_status import post_appointments_status


class AppointmentsApi(Resource):
    """ here is the api endpoint to get, post and delete organizations record"""
    def get(self) -> Response:
        data =dict(request.args)
        return get_appointment(data)

    def put(self) -> Response:
        data = request.get_json(force=True)
        service_id = data.pop("service_id")
        return put_appointments(service_id, data)

    def delete(self) -> Response:
        data = request.get_json(force=True)
        service_id = data.pop("service_id")
        return delete_appointment(service_id, data)
