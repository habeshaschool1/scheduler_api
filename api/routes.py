# flask packages
from flask_restful import Api

# project resources_legacy
from api.appointment import AppointmentsApi
from api.calendar import CalendarApi
from api.comments import CommentsApi
from api.confirm import ConfirmApi
from api.district import DistrictsApi
from api.email import EmailApi
from api.organization import OrganizationsApi
from api.category import CategoriesApi
from api.appointments_status import AppointmentsStatusApi
from api.service import ServicesApi
from api.user import UsersApi, LoginApi, ChangePasswordApi, LogoutApi, EmployeeApi


def create_routes(api: Api):
    """
        this is a routing place for our entier api
    """
    # organization
    api.add_resource(ConfirmApi, '/confirm/through_email')
    api.add_resource(UsersApi, '/users', '/users/public_info', '/current_user')
    api.add_resource(EmployeeApi, '/employees',  '/employees/<user_id>')
    api.add_resource(LoginApi, '/login')
    api.add_resource(ChangePasswordApi, '/change_password')
    api.add_resource(LogoutApi, '/logout')
    api.add_resource(CalendarApi, '/calendar', '/calendar_ethiopia')
    api.add_resource(DistrictsApi, '/districts', '/districts/<district_id>')
    api.add_resource(CategoriesApi, '/categories', '/categories/<category_id>')
    api.add_resource(OrganizationsApi, '/organizations', '/organizations/<organization_id>')
    api.add_resource(ServicesApi, '/services',  '/services/<service_id>')
    api.add_resource(AppointmentsApi, '/appointments')
    api.add_resource(AppointmentsStatusApi, '/appointments_status', '/appointments_status/email')
    api.add_resource(CommentsApi, '/comments', '/comments/<_id>')
    api.add_resource(EmailApi, '/emails')
