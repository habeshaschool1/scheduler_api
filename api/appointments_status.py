# flask packages
from flask import Response, request
from flask_restful import Resource

# project resources_legacy
from helper.h_appointments_status import post_appointments_status, delete_appointments_status, get_appointments_status, \
    put_appointments_status, email_appointments_status


class AppointmentsStatusApi(Resource):
    """ here is the api endpoint to get, post and delete organizations record"""
    def get(self) -> Response:
        data =dict(request.args)
        if "/email" in request.url:
            return email_appointments_status(data)

        return get_appointments_status(data)

    def put(self) -> Response:
        data = request.get_json(force=True)
        return put_appointments_status(data)

    def delete(self) -> Response:
        data = request.get_json(force=True)
        return delete_appointments_status(data)