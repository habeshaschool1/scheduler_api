# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, IntField, DateTimeField, EmailField, ListField, EmbeddedDocument, \
    EmbeddedDocumentField, DecimalField


class Organizations(Document):
    """
    Template for scheduler organization.
    Each schedule should be under one of the organization in organization table
    The schedule page will use this organization table to group and display organizaions
    """

    organization_id = IntField(required=True, default=0)
    pincode = StringField(unique=True)
    name = StringField(max_length=100, required=True)
    address = StringField(required=True)
    desc = StringField(max_length=1000, default="")
    comment = StringField(max_length=1000, default="")
    email = EmailField(unique=True, required=True)
    phone = IntField(unique=True, required=True)
    category_id = IntField(required=True)
    category_name = StringField(required=True)
    district_id = IntField(required=True)
    city = StringField(required=True)
    region = StringField(default="No Region")
    zone = StringField(default="No Zone")
    wereda = StringField(default="No Woreda")
    search_text = StringField(required=False)
    time_format = StringField(default="Ethiopian")
    holidays = ListField(default=["1-30/12", "23/6"])
    requirements = ListField(default=[])
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')


class GroupBy(EmbeddedDocument):
    """
    This is actual report fields about the agenda .
    """
    category_id = IntField()
    district_id = IntField()


class organizations_group(Document):
    """ don't modify this as it should be similar to mongodb view table"""
    _id = EmbeddedDocumentField(GroupBy)
    category_name = StringField()
    city = StringField()
    count = IntField()