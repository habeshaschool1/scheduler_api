# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, IntField, DateTimeField, EmailField, EmbeddedDocumentField, \
    EmbeddedDocument, ListField

class Comments(Document):
    """
    Template for scheduler organization.
    Each schedule should be under one of the organization in organization table
    The schedule page will use this organization table to group and display organizations
    """
    phone = IntField(required=False)
    email = EmailField(required=False)
    comment = StringField(required=True)
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')

