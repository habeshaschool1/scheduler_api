# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, IntField, DateTimeField, EmailField, ListField, EmbeddedDocument, \
    EmbeddedDocumentField, DecimalField


class Districts(Document):
    """
    Template for scheduler organization.
    Each schedule should be under one of the organization in organization table
    The schedule page will use this organization table to group and display organizaions
    """

    district_id = IntField(unique=True)
    city= StringField(required=True, min_length=2)
    region = StringField(default="No Region")
    zone = StringField(default="No Zone")
    wereda = StringField(default="No Woreda")
    info = StringField(required=False)
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')

