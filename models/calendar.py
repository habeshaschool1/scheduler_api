# mongo-engine packages

from mongoengine import Document, StringField, IntField, EmbeddedDocumentField, EmbeddedDocument, BooleanField
from helper.constants import CCalendar

c_calendar = CCalendar()
weekdays = c_calendar.WEEKDAYS
weekdays_ethiopia = CCalendar.WEEKDAYS_ETHIOPIA
months = list(CCalendar.MONTHS.keys())
months_ethiopia = list(CCalendar.MONTHS_ETHIOPIA.keys())


class Calendar(Document):
    month_id = IntField(required=True, unique=True, choices=(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
    month_name = StringField(default=months[0], choices=months, unique=True)
    d1_weekday_name = StringField(default=weekdays[0], choices=weekdays)
    d1_year_day = IntField(default=1)
    size = IntField(required=True, default=30, choices=(28, 30, 31))
    is_active = BooleanField(default=True)
    year = IntField(default=2021, min_value=2021)


class CalendarEthiopia(Document):
    month_id = IntField(required=True, unique=True, choices=(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))
    month_name = StringField(default=months_ethiopia[0], choices=months_ethiopia, unique=True)
    d1_weekday_name = StringField(default=weekdays_ethiopia[0], choices=weekdays_ethiopia)
    d1_year_day = IntField(default=1)
    size = IntField(required=True, default=30, choices=(30, 6, 5))
    is_active = BooleanField(default=True)
    year = IntField(default=2013, min_value=2013)

