# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, IntField, DateTimeField, EmailField, ListField, EmbeddedDocument, \
    EmbeddedDocumentField, DecimalField

from helper.constants import CCalendar
from helper.h_calendar import c_calendar

weekdays = CCalendar().WEEKDAYS
months = list(c_calendar.MONTHS.keys())
months_local = list(c_calendar.MONTHS_ETHIOPIA.keys())


class Day(EmbeddedDocument):
    """
    This is actual report fields about the agenda .
    """
    t01_00_am = IntField(required=True, default=0, min_value=0)
    t01_30_am = IntField(required=True, default=0, min_value=0)
    t02_00_am = IntField(required=True, default=0, min_value=0)
    t02_30_am = IntField(required=True, default=0, min_value=0)
    t03_00_am = IntField(required=True, default=0, min_value=0)
    t03_30_am = IntField(required=True, default=0, min_value=0)
    t04_00_am = IntField(required=True, default=0, min_value=0)
    t04_30_am = IntField(required=True, default=0, min_value=0)
    t05_00_am = IntField(required=True, default=0, min_value=0)
    t05_30_am = IntField(required=True, default=0, min_value=0)
    t06_00_am = IntField(required=True, default=0, min_value=0)
    t06_30_am = IntField(required=True, default=0, min_value=0)
    t07_00_am = IntField(required=True, default=0, min_value=0)
    t07_30_am = IntField(required=True, default=0, min_value=0)
    t08_00_am = IntField(required=True, default=0, min_value=0)
    t08_30_am = IntField(required=True, default=5, min_value=0)
    t09_00_am = IntField(required=True, default=5, min_value=0)
    t09_30_am = IntField(required=True, default=5, min_value=0)
    t10_00_am = IntField(required=True, default=5, min_value=0)
    t10_30_am = IntField(required=True, default=5, min_value=0)
    t11_00_am = IntField(required=True, default=5, min_value=0)
    t11_30_am = IntField(required=True, default=5, min_value=0)
    t12_00_am = IntField(required=True, default=5, min_value=0)
    t12_30_am = IntField(required=True, default=5, min_value=0)
    t01_00_pm = IntField(required=True, default=5, min_value=0)
    t01_30_pm = IntField(required=True, default=5, min_value=0)
    t02_00_pm = IntField(required=True, default=5, min_value=0)
    t02_30_pm = IntField(required=True, default=5, min_value=0)
    t03_00_pm = IntField(required=True, default=5, min_value=0)
    t03_30_pm = IntField(required=True, default=5, min_value=0)
    t04_00_pm = IntField(required=True, default=5, min_value=0)
    t04_30_pm = IntField(required=True, default=5, min_value=0)
    t05_00_pm = IntField(required=True, default=0, min_value=0)
    t05_30_pm = IntField(required=True, default=0, min_value=0)
    t06_00_pm = IntField(required=True, default=0, min_value=0)
    t06_30_pm = IntField(required=True, default=0, min_value=0)
    t07_00_pm = IntField(required=True, default=0, min_value=0)
    t07_30_pm = IntField(required=True, default=0, min_value=0)
    t08_00_pm = IntField(required=True, default=0, min_value=0)
    t08_30_pm = IntField(required=True, default=0, min_value=0)
    t09_00_pm = IntField(required=True, default=0, min_value=0)
    t09_30_pm = IntField(required=True, default=0, min_value=0)
    t10_00_pm = IntField(required=True, default=0, min_value=0)
    t10_30_pm = IntField(required=True, default=0, min_value=0)
    t11_00_pm = IntField(required=True, default=0, min_value=0)
    t11_30_pm = IntField(required=True, default=0, min_value=0)
    t12_00_pm = IntField(required=True, default=0, min_value=0)
    t12_30_pm = IntField(required=True, default=0, min_value=0)


class Services(Document):
    organization_id = IntField(required=True)
    service_id = StringField(unique=True, required=True)
    counter = IntField(required=True)
    name = StringField(default="Select this for any service")
    desc = StringField(max_length=1000, default="")
    period = IntField(required=True, default=30, desc="number of days allowed for appointment from now")
    capacity = EmbeddedDocumentField(Day, default=Day())
    exclude_weekdays = ListField(default=weekdays[-2:], choices=weekdays, desc="default is Monday to Friday")
    exclude_months = ListField(default=[], choices=months, desc="default you are working on all months")
    exclude_months_local = ListField(default=[], choices=months_local, desc="default you are working on all months")
    holidays = ListField(default=[])
    exclude_year_days = ListField(default=[])
    requirements = ListField(default=[])
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')

