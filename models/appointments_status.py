# mongo-engine packages
from datetime import datetime
from mongoengine import Document, StringField, IntField, DateTimeField, EmailField, EmbeddedDocumentField, \
    EmbeddedDocument, ListField

from helper.constants import CAppointmentsStatus

cAppointmentsStatus = CAppointmentsStatus()


class AppointmentsStatus(Document):
    """
    Template for scheduler organization.
    Each schedule should be under one of the organization in organization table
    The schedule page will use this organization table to group and display organizations
    """
    pincode = StringField(unique=True)
    organization_id = IntField(required=True)
    organization_name = StringField(required=True)
    service_id = StringField(required=True)
    service_name = StringField(required=True)
    d = IntField(required=True)
    t = StringField(required=True)
    fullname = StringField(required=True)
    phone = IntField(required=True)
    status = StringField(default=cAppointmentsStatus.ACTIVE, choices=cAppointmentsStatus.ALL_FLAGS)
    reason = StringField(required=False)
    complete_date = StringField(required=False, default="")


