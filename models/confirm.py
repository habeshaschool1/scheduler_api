# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, IntField, DateTimeField, EmailField, ListField, EmbeddedDocument, \
    EmbeddedDocumentField, DecimalField

from helper.constants import DEFAULT_EMAIL, DEFAULT_PHONE


class Confirm(Document):
    """
    Template for scheduler organization.
    Each schedule should be under one of the organization in organization table
    The schedule page will use this organization table to group and display organizaions
    """

    pincode = StringField(unique=True, required=True)
    email = EmailField(default=DEFAULT_EMAIL)
    phone = StringField(default=DEFAULT_PHONE)
    service = StringField(default="")

