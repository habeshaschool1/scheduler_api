# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, IntField, DateTimeField, EmailField, ListField, EmbeddedDocument, \
    EmbeddedDocumentField, DecimalField


class Categories(Document):
    """
    Template for scheduler organization.
    Each schedule should be under one of the organization in organization table
    The schedule page will use this organization table to group and display organizaions
    """

    category_id = IntField(unique=True, required=True)
    name = StringField(unique=True, required=True, min_length=2)
    desc = StringField(default="")
    info = StringField(required=False)
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')

