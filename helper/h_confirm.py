# mongoengine resources_legacy

# project resources_legacy
import json

from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success, server_error
from helper.constants import CConfirm
from helper.operate import generate_string, send_mail
from models.categories import Categories
from models.confirm import Confirm

from models.organizations import Organizations


def get_confirm(pincode, email):
    try:
        confirm = Confirm.objects.get(pincode=pincode, email=email)
        return get_success(result=confirm)
    except Exception as e:
        return client_error(msg=f"{e}")


def post_confirm_by_email(data):
    try:
        # don't send if email already asked the max limit
        confirm = Confirm.objects(email=data['email'])
        if len(confirm) >= CConfirm().MAX_NUMBER_OF_REQUESTS:
            raise Exception(f""" PIN-CODE has already been sent { CConfirm().MAX_NUMBER_OF_REQUESTS} times. 
            Get it from your email {data['email']}, and submit it along with your organization details""")

        pincode = generate_string(upper=6, number=3)
        post_data={"pincode":pincode, 'email':data['email']}
        post_record = Confirm(**post_data)
        post_record.save()

        # send email
        msg = f"""Your new PIN-CODE is: {pincode}  
            Now get the PIN-CODE from your email and submit along with your organization details
            """
        is_mailed = send_mail(data["email"], subject="Ketero, your new organization PIN-CODE", body=msg)
        if is_mailed:
            # send success message
            msg = f""" PIN-CODE has already bee sent to your {data['email']}. 
            Now get the PIN-CODE from your email and submit along with your organization details
            """
            return post_success(msg=msg)
        else:
            # if send mail failed then roll back
            return client_error(msg='Send mail failed, try again')

        return post_success(result=post_record)
    except Exception as e:
        return client_error(msg=f"{e}")


def post_confirm_by_phone(data):
    return server_error(msg="Confirm by phone service not active for now. Use confirm by email for today")


def delete_confirm():
    try:
        category = Categories.objects
        category.delete()
        return delete_success(result=category)
    except Exception as e:
        return client_error(msg=f"{e}")


