# mongoengine resources_legacy

# project resources_legacy
import decimal
import json

from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success
from helper.h_appointments_status import post_appointments_status
from helper.h_calendar import get_year_day
from helper.operate import generate_string

from models.appointments import Appointments
from models.organizations import Organizations
from models.services import Services


def get_appointment(data):
    try:
        # get service data
        m = str(data['m']).capitalize()
        d = int(data['d'])
        service_id = data['service_id']
        # UI is sending year_day, to save finding year day from calendar
        year_day = int(data.get('year_day', get_year_day(m, d)))

        appointments_d = Appointments.objects.get(service_id=service_id)[f"d{year_day}"]
        return get_success(result=appointments_d)
    except Exception as e:
        return client_error(msg=f"{e}")


def post_appointment(data):
    try:
        # add into appointment table
        appointments = Appointments(**data)
        appointments.save()
        return post_success(result=data, msg="organization type has been created successfully")
    except Exception as e:
        return client_error(msg=f"{e}")


def put_appointments(service_id, data):
    # if request is coming from our ui side then we will the complete appointment date
    try:
        # get service data
        service = Services.objects.get(service_id=service_id)
        capacity = service["capacity"]
        service_name = service["name"]
        exclude_months = service["exclude_months"]
        exclude_months_local = service["exclude_months_local"]
        holidays = service['holidays']
        exclude_year_days = service["exclude_year_days"]
        phone_number = data["phone"]
        m = str(data['m']).capitalize()
        d = int(data['d'])
        t = data['t']

        # UI is sending year_day, to save finding year day from calendar
        year_day = int(data.get('year_day', get_year_day(m, d)))
        complete_date = data.pop('complete_date', f"{m} {d}, {t}")
        if year_day in exclude_year_days :
            return client_error(msg=f"Sorry, we are not working on {complete_date}")

        appointment = Appointments.objects.get(service_id=service_id)
        appointments_d = appointment[f"d{d}"]
        appointments_t = set(appointments_d[t])

        # check if fullname or phone number is already on that day
        fullname = " ".join(data['fullname'].title().split())
        appointments_d_text = json.dumps(appointments_d.to_json())
        if fullname in appointments_d_text or str(phone_number) in appointments_d_text:
            return client_error(result=data, msg=f"You already have appointment on this day of this organization. For more you may go to my appointments page and see details")

        # check if not free slot
        if len(appointments_t) >= capacity[t]:
            return client_error(msg=f"Someone has taken soon before you hit. Pick other available time and book it soon")

        # post the appointment to the right date and time
        pincode = generate_string(upper=6, number=3) + f"_{year_day}"
        appointments_t.add(f"{fullname}, {phone_number}, {pincode}")
        update_data = {f"set__d{year_day}__{t}": appointments_t}
        appointment.update(**update_data)

        # save to appointment_status table, if not then remove from appointment again
        # get organization
        organization_id = int(data['organization_id'])
        organization = Organizations.objects.get(organization_id=organization_id)
        organization_name = organization['name']
        data_appointment_status = {
            'organization_id': organization_id,
            'organization_name': organization_name,
            'service_id': service_id,
            'service_name': service_name,
            'd': d,
            't': t,
            'fullname': fullname,
            'phone': data['phone'],
            'pincode': pincode,
            'complete_date': complete_date
        }

        resp = post_appointments_status(data_appointment_status)

        if resp.status_code >= 400:
            appointments_t.remove(phone_number)
            update_data = {f"set__d{year_day}__{t}": appointments_t}
            appointment.update(**update_data)
            return resp

        return put_success(result=data_appointment_status, msg="booked successfully")

    except Exception as e:
        return client_error(msg=f"{e}")


def delete_appointment(service_id, data):
    try:
        # TODO this should filter phone by m, d, and t, and delete it
        d = data['d']
        t = data['t']
        pincode = data['pincode']
        appointment = Appointments.objects.get(service_id=service_id)
        appointments_d = appointment[d]
        appointments_t = set(appointments_d[t])

        # remove the appointment record form this time list
        temp_set = appointments_t
        for temp  in temp_set:
            if pincode in temp:
                appointments_t.remove(temp)
                break

        update_data = {f"set__d{d}__{t}": appointments_t}
        appointment.update(**update_data)
        return post_success(msg="Appointment has been deleted successfully")
    except Exception as e:
        return not_found(result=service_id, msg=f'{e}')


def delete_service_from_appointment_table(service_id):
    # This to delete an entire service of an organization table
    try:
        appointment = Appointments.objects(service_id=service_id)
        appointment.delete()
        return delete_success(result=appointment)
    except Exception as e:
        return client_error(msg=f"{e}")


def delete_organization_from_appointment_table(organization_id):
    # This is to delete the organization itself from appointments table
    try:
        appointment = Appointments.objects(organization_id=organization_id)
        appointment.delete()
        return delete_success(result=appointment)
    except Exception as e:
        return client_error(msg=f"{e}")
