
from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success, server_error
from helper.constants import CDistrict, CUser
from helper.operate import find_available_number
from models.appointments import Appointments
from models.confirm import Confirm
from models.districts import Districts

from models.organizations import Organizations
from models.users import Users

c_district = CDistrict()
excludes = c_district.TITLE_EXCLUDES
includes = c_district.TITLE_INCLUDES

c_user = CUser()


def get_districts(query={}):
    try:
        districts = Districts.objects(**query)
        return get_success(result=districts)
    except Exception as e:
        return server_error(msg=f'{e}')


def post_district(data):
    try:  # create district
        """ generate a new ID for this new post ( organization) """
        # try if district already exists
        data['city'] = " ".join(data['city'].title().split())
        districts = Districts.objects(**{'city': data['city']})
        if districts:
            return get_success(result=districts, msg=f"District City='{data['city']}' already exists. Check it in the list box")

        # post district
        districts = Districts.objects
        district_id = districts.values_list("district_id")
        data["district_id"] = find_available_number(exclude_list=district_id, min_number=c_district.MIN_NUMBER_OF_DISTRICTS, max_number=c_district.MAX_NUMBER_OF_DISTRICTS)
        post_record = Districts(**data)
        post_record.save()
        return post_success(result=post_record, msg="District has been created successfully, now you can select from the list and use it")
    except Exception as e:
        return client_error(msg=f"{e}")


def put_district(district_id, data: dict):
    try:
        if data.get('city', False):
            data['city'] = " ".join(data['city'].title().split())
        # district id can't be updated
        data.pop("district_id", None)
        district = Districts.objects(district_id=district_id)
        district.update(**data)
        return put_success(result=district)
    except Exception as e:
        return client_error(msg=f"{e}")


def delete_district(district_id):
    try:
        district = Districts.objects(district_id=district_id)
        district.delete()
        return delete_success(result=district, msg="district has been deleted successfully")
    except Exception as e:
        return not_found(result=district_id, msg=f"{e}")

