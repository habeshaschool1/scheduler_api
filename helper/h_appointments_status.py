from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success, server_error
from helper.operate import generate_string, send_mail

from models.appointments_status import AppointmentsStatus


def get_appointments_status(data):
    try:
        appointments_status = AppointmentsStatus.objects(**data)
        return get_success(result=appointments_status)
    except Exception as e:
        return client_error(msg=f"{e}")


def email_appointments_status(data):
    try:
        email = data.pop('email')
        appointments_status = AppointmentsStatus.objects(**data)
        if appointments_status:
            appointments_status = appointments_status[0]
            msg = f"""You have appointment for 
            Service: '{appointments_status['service_name']}' 
            Organization: '{appointments_status['organization_name']}'
            Appointment date: {appointments_status['complete_date']}
            PIN-CODE: {appointments_status['pincode']}, 
            Full Name : {appointments_status['fullname']}
            Phone : {appointments_status['phone']}
            Appointment_status : {appointments_status['status']}"""
            is_mailed = send_mail(email, subject="Ketero, confirmation for your appointment", body=msg)
            if is_mailed:
                return get_success(result=appointments_status,
                                   msg=f"Your appointment has been sent to your email {email}")
            else:
                return server_error(result=[],
                                    msg=f"Sorry, unable to send this confirmation to your email {email} try again later or use other alternatives like print and google notification")
    except Exception as e:
        return client_error(msg=f"{e}")


def post_appointments_status(data: dict):
    try:
        # post to appointments_status appointment
        appointments_status = AppointmentsStatus(**data)
        appointments_status.save()
        return put_success(result=data, msg="appointment has been added into status control table")
    except Exception as e:
        return client_error(msg=f"{e}")


def put_appointments_status(data):
    try:
        appointments_status = AppointmentsStatus.objects(service_id=data['service_id'], pincode=data['pincode'])
        put_data = {"status": data['status'], "reason": data['reason']}
        appointments_status.update(**put_data)
        return put_success(result=appointments_status, msg=f" appointment has been added to status control")
    except Exception as e:
        return client_error(msg=f"{e}")


def delete_appointments_status(data):
    try:
        pincode = data.pop('phone')
        # TODO this should take appointment id too
        appointment = AppointmentsStatus.objects(pincode=pincode)
        appointment.delete()
        return delete_success(result=appointment)
    except Exception as e:
        return not_found(result=data, msg=f'{e}')


def delete_organization_from_appointments_table(organization_id):
    try:
        # TODO make a backup before deleting
        appointment = AppointmentsStatus.objects(organization_id=organization_id)
        resp = appointment.delete()
        return delete_success(result=appointment)
    except Exception as e:
        return not_found(result={"organization_id": organization_id}, msg=f'{e}')
