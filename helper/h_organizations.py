# mongoengine resources_legacy

# project resources_legacy
import json
import unicodedata

from flask_jwt_extended import jwt_required

from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success, server_error
from helper.constants import COrganization, URL_HOME_PAGE, CONTACT_INFORMATION_MSG, CUser
from helper.h_comments import post_comment
from helper.h_services import post_service
from helper.h_users import post_user
from helper.operate import find_available_number, send_mail, generate_string
from models.appointments import Appointments
from models.categories import Categories
from models.confirm import Confirm
from models.districts import Districts

from models.organizations import Organizations, organizations_group
from models.services import Services
from models.users import Users

c_organization = COrganization()
excludes = c_organization.TITLE_EXCLUDES
includes = c_organization.TITLE_INCLUDES


def get_organizations(organization_id=None, query={}):
    try:
        if organization_id is not None:
            organizations = Organizations.objects.get(organization_id=organization_id)
        else:
            organizations = Organizations.objects(**query)
        return get_success(result=organizations)
    except Exception as e:
        return client_error(msg=f"{e}")


def get_organization_group(organization_id=None, query={}):
    try:  # go get organization group by city and category
        summery = organizations_group.objects()
        return get_success(result=summery)
    except Exception as e:
        return client_error(msg=f"{e}")


def post_organization(data):
    # check confirm pincode
    try:
        confirm = Confirm.objects.get(pincode=data['pincode'])
        data['email'] = confirm['email']
    except:
        return client_error(msg="pincode not matching, make sure the pincode is not older than 8 hrs")

    try:
        data['name'] = " ".join(data['name'].split())
        password = data.pop('password')

        # post category
        category = Categories.objects.get(category_id=data['category_id'])
        data['category_id'] = category['category_id']
        data['category_name'] = category['name']

        # get district
        district = Districts.objects.get(district_id=data['district_id'])
        data['district_id'] = district['district_id']
        data['city'] = district['city']
        data['region'] = district['region']
        data['zone'] = district['zone']
        data['wereda'] = district['wereda']

        # post organization
        organizations = Organizations.objects
        organizations_id = organizations.values_list("organization_id")
        organization_id = find_available_number(exclude_list=organizations_id, max_number=c_organization.MAX_NUMBER_OF_ORGANIZATIONS)
        data["organization_id"] = organization_id
        post_record = Organizations(**data)
        post_record.save()

        # post user
        user_data= {'email':data['email'], 'phone': data['phone'], 'password': password, 'organization_id': data["organization_id"], 'pincode': data['pincode']}
        user_resp = post_user(user_data)

        # post default service
        service_data = {
            "name": c_organization.DEFAULT_SERVICE_NAME,
            "organization_id": organization_id
        }
        post_service(service_data)

        # return the admin user account, so that system will autologin for the user
        return user_resp

    except Exception as e:
        return client_error(msg=f"{e}")


def put_organization(organization_id, data: dict):
    try:
        if data.get('name', False):
            data['name'] = " ".join(data['name'].split())

        data.pop("organization_id", None)
        category = Categories.objects.get(category_id=data['category_id'])
        data['category_name'] = category['name']
        district = Districts.objects.get(district_id=data['district_id'])
        data['city'] = district['city']
        data['region'] = district['region']
        data['zone'] = district['zone']
        data['wereda'] = district['wereda']
        organization = Organizations.objects(organization_id=organization_id)
        organization.update(**data)
        return put_success(result=organization)
    except Exception as e:
        return client_error(msg=f"{e}")


def delete_organization(organization_id):
    try:
        exceptions = ""
        organizations = Organizations.objects(organization_id=organization_id)
        name = organizations[0]['name']
        email = organizations[0]['email']
        phone = organizations[0]['phone']
        organizations.delete()
        exceptions = f"{exceptions}\nDeleting organization exception ==> organization_name: {name}, email:{email}, phone:{phone}"

        try:
            users = Users.objects(organization_id=organization_id)
            users.delete()
        except Exception as e:
            exceptions = f"{exceptions}\nDeleting users>{e}"

        try:
            services = Services.objects(organization_id=organization_id)
            services.delete()
        except Exception as e:
            exceptions = f"{exceptions}\n deleting service,  {e}"

        try:
            appointments = Appointments.objects(organization_id=organization_id)
            appointments.delete()
        except:
            exceptions = f"{exceptions}\nDeleting appointments, >{e}"
        # post to comment that deleting organization counters an issue
        try:
            post_data = {
                "phone": phone,
                "email": email,
                "comment": exceptions
            }
            post_comment(post_data)
        except:
            pass

        return delete_success(msg="organization, with its users, services and appointments has been deleted permanently")

    except Exception as e:
        return not_found(result=organization_id, msg=f"{e}")

