import os

URL_HOME_PAGE = "http://127.0.0.1:8888"
URL_LOGIN_PAGE = f"{URL_HOME_PAGE}/login"
URL_REGISTER_PAGE = f"{URL_HOME_PAGE}/register"
EMAIL = "scheduler200@gmail.com"
EMAIL_PASSWORD = "Ketero4me."
CONTACT_INFORMATION_MSG = """
 Copyright &copy; 2021 | Software by Codifire | All Rights Reserved
 (US)+1-301-792-9864
 (Et)+251-91082032
"""

ROOT_DIR = os.path.dirname(os.path.abspath("api"))
SECRET = False  # if true then user ID will be unknown while voting, to hide who vote what
PASSWORD_STRENGTH_LEVEL = 1  # 1 (must have any value), 2(length should be >=6), 3(must have at least 1 from each (special, upper, number)
IMAGE_EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif']
IMAGE_MAX_SIZE_KB = 300
PROFILE_IMAGE_MAX_SIZE_KB = 200

# defaults
DEFAULT_EMAIL = 'none@ketero.com'
DEFAULT_PASSWORD = "test123"
DEFAULT_PHONE = '251910101010'

TITLE_INCLUDES = ""
TITLE_MAY_INCLUDES = '-_?!.:,'
TITLE_EXCLUDES = '!@#$%^&*()+={}\|<>;'

CHOICE_SPLITTER = "***"
CHOICE_INCLUDES = ""
CHOICE_MAY_INCLUDES = '-_?!.:,'
CHOICE_EXCLUDES = '!@#$%^&*()+={}\|<>;'


class CConfirm:
    MAX_NUMBER_OF_REQUESTS = 3


class CCalendar:
    MONTH_SIZES = (28, 29, 30, 31)
    LOCAL_MONTH_DAYS_DIFF = (6, 7, 8, 9, 10, 11, 12)

    WEEKDAYS = ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    WEEKDAYS_ETHIOPIA = ("ሰኞ", "ማክሰኞ", "እሮብ", "ሃሙስ", "ዓርብ", "ቅዳሜ", "እሁድ")
    MONTHS = {"January": {"month_id": 1, "size": 31, 'd1_year_day': 1},
              "February": {"month_id": 2, "size": 28, 'd1_year_day': 32},
              "March": {"month_id": 3, "size": 31, 'd1_year_day': 60},
              "April": {"month_id": 4, "size": 30, 'd1_year_day': 91},
              "May": {"month_id": 5, "size": 31, 'd1_year_day': 121},
              "Jun": {"month_id": 6, "size": 30, 'd1_year_day': 152},
              "July": {"month_id": 7, "size": 31, 'd1_year_day': 182},
              "August": {"month_id": 8, "size": 31, 'd1_year_day': 213},
              "September": {"month_id": 9, "size": 30, 'd1_year_day': 244},
              "October": {"month_id": 10, "size": 31, 'd1_year_day': 274},
              "November": {"month_id": 11, "size": 30, 'd1_year_day': 305},
              "December": {"month_id": 12, "size": 31, 'd1_year_day': 335}
              }

    MONTHS_ETHIOPIA = {"መስከረም": {"month_id": 1, "size": 30, 'd1_year_day': 250},
                       "ጥቅምት": {"month_id": 2, "size": 30, 'd1_year_day': 280},
                       "ህዳር": {"month_id": 3, "size": 30, 'd1_year_day': 310},
                       "ታህሳስ": {"month_id": 4, "size": 30, 'd1_year_day': 340},
                       "ጥር": {"month_id": 5, "size": 30, 'd1_year_day': 5},
                       "የካቲት": {"month_id": 6, "size": 30, 'd1_year_day': 35},
                       "መጋቢት": {"month_id": 7, "size": 30, 'd1_year_day': 65},
                       "ሚያዚያ": {"month_id": 8, "size": 30, 'd1_year_day': 95},
                       "ግንቦት": {"month_id": 9, "size": 30, 'd1_year_day': 125},
                       "ሰኔ": {"month_id": 10, "size": 30, 'd1_year_day': 155},
                       "ሃምሌ": {"month_id": 11, "size": 30, 'd1_year_day': 185},
                       "ነሃሴ": {"month_id": 12, "size": 30, 'd1_year_day': 215},
                       "ጷጉሜ": {"month_id": 13, "size": 5, 'd1_year_day': 245}
                       }

    DEFAULT_HOLIDAYS = ['September 01',  # new year ==> September 11
                        'September 17',  # meskel ==> September 27
                        'October 08',  # Moulid ==> October 18
                        'December 29',  # ledet ==> January 7
                        'January 11',  # Timket ==> January 19
                        'February 23',  # adewa ==> March 2
                        'March 19',  # romedan ==> March 28
                        'April 14',  # Seklet ==> April 22
                        'April 16',  # Fasika ==>  April 24
                        'April 23',  # wezader ken ==>  May 1
                        'April 24',  # Remedan ken ==>  May 2
                        'April 27',  # Arbegnoch ken ==>  May 5
                        'July 2',  # Arefa ==> July  9
                        ]

    LOCAL_TIME = {
        't08_00_am': 'ጥዋት 02:00',
        't08_30_am': 'ጥዋት 02:30',
        't09_00_am': 'ጥዋት 03:00',
        't09_30_am': 'ጥዋት 03:30',
        't10_00_am': 'ቀን 04:00',
        't10_30_am': 'ቀን 04:30',
        't11_00_am': 'ቀን 05:00',
        't11_30_am': 'ቀን 05:30',
        't12_00_am': 'ቀን 06:00',
        't12_30_am': 'ቀን 06:30',
        't01_00_pm': 'ቀን 07:00',
        't01_30_pm': 'ቀን 07:30',
        't02_00_pm': 'ቀን 08:00',
        't02_30_pm': 'ቀን 08:30',
        't03_00_pm': 'ቀን 09:00',
        't03_30_pm': 'ቀን 09:30',
        't04_00_pm': 'ቀን 10:00',
        't04_30_pm': 'ቀን 10:30',
        't05_00_pm': 'ቀን 11:00',
        't05_30_pm': 'ቀን 11:30',
        't06_00_pm': 'ምሽት 12:00',
        't06_30_pm': 'ምሽት 12:30',
        't07_00_pm': 'ምሽት 01:00',
        't07_30_pm': 'ምሽት 01:30',
        't08_00_pm': 'ምሽት 02:00',
        't08_30_pm': 'ምሽት 02:30',
        't09_00_pm': 'ምሽት 03:00',
        't09_30_pm': 'ምሽት 03:30',
        't10_00_pm': 'ምሽት 04:00',
        't10_30_pm': 'ምሽት 04:30',
        't11_00_pm': 'ምሽት 05:00',
        't11_30_pm': 'ምሽት 05:30',
        't12_00_pm': 'ለሊት 06:00',
        't12_30_pm': 'ለሊት 06:30',
        't01_00_am': 'ለሊት 07:00',
        't01_30_am': 'ለሊት 07:30',
        't02_00_am': 'ለሊት 08:00',
        't02_30_am': 'ለሊት 08:30',
        't03_00_am': 'ለሊት 09:00',
        't03_30_am': 'ለሊት 09:30',
        't04_00_am': 'ንጋት 10:00',
        't04_30_am': 'ንጋት 10:30',
        't05_00_am': 'ንጋት 11:00',
        't05_30_am': 'ንጋት 11:30',
        't06_00_am': 'ጥዋት 12:00',
        't06_30_am': 'ጥዋት 12:30',
        't07_00_am': 'ጥዋት 01:00',
        't07_30_am': 'ጥዋት 01:30'

    }


class CUser:
    ACCESS_SYSTEM_USER = 'system'
    ACCESS_ADMIN_USER = 'admin'
    ACCESS_NORMAL_USER = 'user'
    USER_ALL_ACCESS_FLAGS = (ACCESS_SYSTEM_USER, ACCESS_ADMIN_USER, ACCESS_NORMAL_USER)

    USER_MIN_ID = 100000
    USER_MAX_ID = 999999

    # during registration, it will determine the user behaviour active/inactive
    SET_USER_ACTIVE = True


class CCategory:
    MIN_NUMBER_OF_CATEGORIES = 10
    MAX_NUMBER_OF_CATEGORIES = 99
    TITLE_EXCLUDES = TITLE_EXCLUDES
    TITLE_INCLUDES = TITLE_INCLUDES
    TITLE_MAY_INCLUDES = TITLE_MAY_INCLUDES


class CDistrict:
    MIN_NUMBER_OF_DISTRICTS = 100
    MAX_NUMBER_OF_DISTRICTS = 999
    TITLE_EXCLUDES = TITLE_EXCLUDES
    TITLE_INCLUDES = TITLE_INCLUDES
    TITLE_MAY_INCLUDES = TITLE_MAY_INCLUDES


class COrganization:
    MIN_NUMBER_OF_ORGANIZATIONS = 1000
    MAX_NUMBER_OF_ORGANIZATIONS = 9999
    TITLE_EXCLUDES = TITLE_EXCLUDES
    TITLE_INCLUDES = TITLE_INCLUDES
    TITLE_MAY_INCLUDES = TITLE_MAY_INCLUDES
    DEFAULT_SERVICE_NAME = "General Service"


class CServices:
    MIN_NUMBER_OF_SERVICES = 1000
    MAX_NUMBER_OF_SERVICES = 9999
    TITLE_EXCLUDES = TITLE_EXCLUDES
    TITLE_INCLUDES = TITLE_INCLUDES
    TITLE_MAY_INCLUDES = TITLE_MAY_INCLUDES


class CAppointmentsStatus:
    ACTIVE = 'Active'
    CANCELLED = 'Cancelled'
    REOPENED = 'Reopened'
    PROCESSED = 'Processed'
    ALL_FLAGS = (ACTIVE, CANCELLED, REOPENED, PROCESSED)
