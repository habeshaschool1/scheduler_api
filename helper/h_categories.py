# mongoengine resources_legacy

# project resources_legacy
import json

from flask_jwt_extended import get_jwt_identity

from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success
from helper.constants import COrganization, CCategory, CUser
from helper.operate import find_available_number
from models.appointments import Appointments
from models.categories import Categories
from models.confirm import Confirm

from models.organizations import Organizations
from models.users import Users

c_category = CCategory()
excludes = c_category.TITLE_EXCLUDES
includes = c_category.TITLE_INCLUDES

c_user = CUser()


def get_category(query={}):
    try:
        categories = Categories.objects(**query)
        return get_success(result=categories)
    except Exception as e:
        return client_error(msg=f"{e}")


def post_category(data):
    try:  # create the category
        """ generate a new ID for this new post ( organization) """
        # try if category already exists
        data['name'] = " ".join(data['name'].title().split())
        categories = Categories.objects(**{'name': data['name']})
        if categories:
            return get_success(result=categories, msg=f"Category name: '{data['name']}' already exists. Check it in the list box")

        # post category
        categories = Categories.objects
        category_id = categories.values_list("category_id")
        data["category_id"] = find_available_number(exclude_list=category_id, min_number=c_category.MIN_NUMBER_OF_CATEGORIES, max_number=c_category.MAX_NUMBER_OF_CATEGORIES)
        post_record = Categories(**data)
        post_record.save()
        return post_success(result=post_record, msg="Category has been created successfully, now you can select from the list and use it")
    except Exception as e:
        return client_error(msg=f"{e}")


def put_category(category_id, data: dict):
    try:
        if data.get('name', False):
            data['name'] = " ".join(data['name'].title().split())
        # category id cant be updated
        data.pop("category_id", None)
        category = Categories.objects(category_id=category_id)
        category.update(**data)
        return put_success(result=category)
    except Exception as e:
        return client_error(msg=f"{e}")


def delete_category(category_id):
    try:
        category = Categories.objects(category_id=category_id)
        category.delete()
        return delete_success(result=category, msg="category has been deleted successfully")
    except Exception as e:
        return client_error(msg=f"{e}")


