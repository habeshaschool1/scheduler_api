# mongoengine resources_legacy

# project resources_legacy
import json

from flask_jwt_extended import get_jwt_identity

from api.response_messages import post_success, client_error
from helper.operate import send_mail


def post_email(data):
    try:  # just send email
        is_mailed = send_mail(data["email"], subject="Ketero.com, thank you for your comment", body=data['msg'])
        if is_mailed:
            return post_success(result=data, msg='emailed successfully')
        else:
            # if send mail failed then roll back
            return client_error(msg='Send mail failed, try again')
    except Exception as e:
        return client_error(msg=f"{e}")
