
from api.response_messages import client_error, put_success, get_success, not_found, delete_success
from helper.operate import generate_string
from models.comments import Comments


def get_comments(data):
    try:
        comments = Comments.objects(**data)
        return get_success(result=comments)
    except Exception as e:
        return client_error(msg=f"{e}")


def post_comment(data: dict):
    try:
        # post to comments
        comments = Comments(**data)
        comments.save()
        return put_success(result=data, msg="We have received your comment, will see it and reply to you shortly")
    except Exception as e:
        return client_error(msg=f"{e}")


def delete_comment(_id):
    try:
        data = {'_id':{'$oid':_id}}
        comment = Comments.objects(data)
        comment.delete()
        return delete_success(result=comment, msg="comment has been deleted successfully")
    except Exception as e:
        return client_error(msg=f"{e}")


