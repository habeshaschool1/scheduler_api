# mongoengine resources_legacy

# project resources_legacy


from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success
from helper.constants import CCalendar
from models.calendar import Calendar, CalendarEthiopia
import json
import datetime


c_calendar = CCalendar()


def get_calendar(query={}):
    try:
        calendar = Calendar.objects(**query).order_by('+year', '+d1_year_day').exclude("id").to_json()
        calendar = json.loads(calendar)
        return get_success(result=calendar)
    except Exception as e:
        return client_error(msg=f"{e}")


def get_calendar_ethiopia(query={}):
    try:
        calendar = CalendarEthiopia.objects(**query).order_by('+year', '+month_id', '+d1_year_day').exclude("id").to_json()
        calendar = json.loads(calendar)
        return get_success(result=calendar)
    except Exception as e:
        return client_error(msg=f"{e}")


def post_calendar():
    weekdays = c_calendar.WEEKDAYS
    months = c_calendar.MONTHS

    # about today
    today = datetime.datetime.now()
    today_day_of_year = today.timetuple().tm_yday
    new_year = datetime.date(today.year, 1, 1)

    this_year_start_weekday_name = new_year.strftime("%A")  # gives weekday name like Monday, Tuesday

    try:
        index_of_year_start_weekday = weekdays.index(this_year_start_weekday_name)
        adjusted_weekdays = weekdays[index_of_year_start_weekday:] + weekdays[:index_of_year_start_weekday]
        Calendar.objects().delete()
        for month_name, month_data in months.items():
            month_data['month_id'] = month_data['month_id']
            month_data['month_name'] = month_name
            d1_weekday = month_data['d1_year_day'] % 7 - 1
            month_data['d1_weekday_name'] = adjusted_weekdays[d1_weekday]

            # set year to next year if today's year day is greater than current month last day year day
            month_data['year'] = today.year
            if today_day_of_year > month_data['d1_year_day'] + month_data['size']:
                month_data['year'] = today.year + 1

            post_calendar = Calendar(**month_data)
            post_calendar.save()
        calendar = json.loads(Calendar.objects().exclude('id').to_json())

        # update ethiopian calendar
        ethiopia_new_year_year_day = c_calendar.MONTHS_ETHIOPIA['መስከረም']['d1_year_day']
        ethiopia_new_year = new_year + datetime.timedelta(days=ethiopia_new_year_year_day - 1)
        ethiopia_new_year_start_weekday_index = ethiopia_new_year.weekday()  # gives index of weekday, example for Monday = 0
        ethiopia_year = ethiopia_new_year.year - 8

        ethiopia_data = {
            'year_start_weekday_index': ethiopia_new_year_start_weekday_index,
            'year': ethiopia_year,
            'today_day_of_year': today_day_of_year
        }
        post_calendar_ethiopia(ethiopia_data)

        return put_success(result=calendar, msg="calendar has been refreshed successfully")
    except Exception as e:
        return client_error(msg=f"{e}")


def post_calendar_ethiopia(data):
    index_of_month_start_weekday = data['year_start_weekday_index']
    this_year = data['year']
    today_day_of_year = data['today_day_of_year']
    weekdays = c_calendar.WEEKDAYS_ETHIOPIA
    months_ethiopia = c_calendar.MONTHS_ETHIOPIA
    meskerem_1_day_of_year = months_ethiopia["መስከረም"]['d1_year_day']

    try:
        CalendarEthiopia.objects().delete()
        for month_name, month_data in months_ethiopia.items():
            month_data['month_id'] = month_data['month_id']
            month_data['month_name'] = month_name
            index_of_current_month_start_weekday = index_of_month_start_weekday % 7
            month_data['d1_weekday_name'] = weekdays[index_of_current_month_start_weekday]
            month_data['year'] = this_year

            # if current month end date year day is greater than today year day =>  move the current month year to next year
            # if current month end date year day is greater than meskerem end year day  =>  move the current month year to next year
            month_data['year'] = this_year
            this_month_end_year_day = month_data['d1_year_day'] + month_data['size']
            if this_month_end_year_day < today_day_of_year or meskerem_1_day_of_year < this_month_end_year_day:
                month_data['year'] = this_year + 1

            post_calendar = CalendarEthiopia(**month_data)
            post_calendar.save()

            # since ethiopian calendar has 30days each month, the next month start week day will be +2 of the previous month
            # for example if previous month start weekday is on Wednesday, then next month will start on Friday
            index_of_month_start_weekday = index_of_month_start_weekday + 2
        calendar = json.loads(CalendarEthiopia.objects().exclude('id').to_json())
        return put_success(result=calendar, msg="calendar has been refreshed successfully")
    except Exception as e:
        return client_error(msg=f"{e}")


def extract_year_day_to_month_day(year_day):
    """ this method if you give month name ( can be english or amharic) then it will give you year days of that month
    if you add month dya then it will give you year day of that month of that day
    """
    if year_day == 0:
        return None, None
    for month_name, data in c_calendar.MONTHS.items():
        if year_day <= data['d1_year_day'] + data['size']:
            month_day = year_day - data['d1_year_day']
            return month_name, month_day

    return None, None


def get_year_day(month_name, month_day):
    """ get year day for a given month and day """
    if month_day == 0:
        return None
    data = c_calendar.MONTHS.get(month_name, c_calendar.MONTHS_ETHIOPIA.get(month_name, None))
    if data is None:
        return None

    year_day = data['d1_year_day'] + month_day - 1
    return year_day


def get_year_days_of_month(selected_month_name, month_day=0):
    """ this method if you give month name ( can be english or amharic) then it will give you year days of that month
    if you add month day then it will give you year day of that month of that day
    """
    year_days = []
    calendar_data = c_calendar.MONTHS
    # change calendar data to ethiopian if it is ethiopian calendar
    if selected_month_name in c_calendar.MONTHS_ETHIOPIA.keys():
        calendar_data = c_calendar.MONTHS_ETHIOPIA

    if month_day > 0:
        return calendar_data[selected_month_name]['d1_year_day'] + month_day - 1

    for month_name, data in calendar_data.items():
        if selected_month_name in month_name:
            for current_month_day in range(data['size']):
                current_year_day = data['d1_year_day'] + current_month_day
                year_days.append(current_year_day)

    return year_days
