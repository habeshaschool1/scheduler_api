# mongoengine resources_legacy

# project resources_legacy
from flask_jwt_extended import jwt_required

from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success, server_error
from helper.constants import CServices
from helper.h_appointment import post_appointment
from helper.h_calendar import get_year_days_of_month
from models.services import Services

c_services = CServices()


def get_services(service_id=None, query={}):
    try:
        if service_id is not None:
            service = Services.objects.get(service_id=service_id)
            return get_success(result=service)

        services = Services.objects(**query)
        return get_success(result=services)
    except Exception as e:
        return client_error(msg=f"{e}")


def post_service(data):
    try:
        data['name'] = " ".join(data['name'].split())
        organization_id = data['organization_id']
        services = Services.objects(organization_id=organization_id)
        counter = max(services.values_list("counter") or [0]) + 1
        data["service_id"] = f"{organization_id}.{counter}"
        data["organization_id"] = organization_id
        data["counter"] = counter
        post_record = Services(**data)
        post_record.save()

        # post appointemnt
        data = {'organization_id': organization_id, 'service_id': data["service_id"]}
        post_appointment(data)
        return post_success(result=post_record, msg="service has been added successfully")

    except Exception as e:
        return client_error(msg=f"{e}")


def put_service(service_id, data: dict):
    try:
        if data.get('name', False):
            data['name'] = " ".join(data['name'].split())

        # prepare exclude_year_days
        exclude_year_days =[]
        for month_name in data['exclude_months']:
            exclude_year_days = exclude_year_days + get_year_days_of_month(month_name)
        for month_name in data['exclude_months_local']:
            exclude_year_days = exclude_year_days + get_year_days_of_month(month_name)
        for month_name_and_day in data['holidays']:
            month_name, month_day = month_name_and_day.split(" ")
            exclude_year_days = exclude_year_days + [get_year_days_of_month(month_name, int(month_day))]
        data['exclude_year_days'] = exclude_year_days

        service = Services.objects(service_id=service_id)
        service.update(**data)
        return put_success(result=service)
    except Exception as e:
        return client_error(msg=f"{e}")


def delete_service(service_id):
    try:
        service = Services.objects(service_id=service_id)
        service.delete()
        return delete_success(result=[{"service_id": service_id}], msg=f"service {service_id} has been deleted ")
    except Exception as e:
        return not_found(result={'service_id':service_id}, msg=f"{e}")

